<?php

use App\Todo;
use App\User;
use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->delete(); // Todo: remove on production!

        factory(App\Todo::class, 20)->create();
    }
}
