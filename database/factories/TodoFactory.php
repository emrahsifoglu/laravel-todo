<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Todo::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(2, 20),
        'title' => $faker->word,
        'description' => $faker->sentence(10),
        'content' => implode(' ', $faker->paragraphs(2)),
        'due_at' => \Carbon\Carbon::now()->addWeek(),
        'finished_at' => null,
    ];
});
