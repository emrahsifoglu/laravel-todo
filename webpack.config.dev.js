const { resolve } = require('path');
const { DefinePlugin } = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.config.common');

module.exports = merge(common, {
    devtool: 'inline-source-map',
    context: resolve(__dirname, 'resources/assets/ts'),
    entry: [
        './index.tsx',
    ],
    plugins: [
        new DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
    ],
});
