import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import createHistory from 'history/createBrowserHistory';
import { configureStore } from 'redux/store';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import 'react-toastify/dist/ReactToastify.css';
import '../sass/app.scss';
import '../css/app.css';
import 'bootstrap/dist/js/bootstrap';


const history = createHistory();

const store = configureStore(history, (window as any).__INITIAL_STATE__);

const render = () =>
    ReactDOM.render(
        <Provider store={store}>
            <Router history={history}>
                <App />
            </Router>
        </Provider>,
        document.getElementById('app')
    );

declare const module: any;

if (module.hot) {
    module.hot.accept();
    render();
} else {
    render();
}

if (process.env.NODE_ENV !== 'production') {
    registerServiceWorker();
}
