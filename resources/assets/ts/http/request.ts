import * as _ from 'lodash';
import axios, { AxiosRequestConfig } from 'axios';
import { Promise } from 'es6-promise';
import { IDictionary } from 'common/data';

const client = axios.create();

export interface RequestOptions {
    headers?: { [h: string]: string };
}

type RequestMethod = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

export function request(method: RequestMethod , url: string, data?: IDictionary<any>, opts?: RequestOptions) {

    const defaultHeaders = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    };

    const requestConfig: AxiosRequestConfig = {
        method,
        url,
        data,
        headers: defaultHeaders
    };

    if (opts && opts.headers) {
        requestConfig.headers = _.assign({}, requestConfig.headers, opts.headers);
    }

    return Promise.resolve()
        .then(() => client(requestConfig))
        .catch((error) => {
            return Promise.reject<string>(error.response || error.message);
        });
}
