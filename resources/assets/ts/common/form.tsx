import * as React from 'react';
import { Field } from 'redux-form';
import * as classNames from 'classnames';

export const renderInput = (field: any) => {
    const hasError = !!field.meta.error && !!field.meta.touched;
    const classNameArray = ['form-group', { 'has-error': hasError }];
    const classNamesResult = classNames(classNameArray);

    return (
        <div className={classNamesResult}>
            <label className="control-label" htmlFor={field.id}>{field.placeholder}</label>
            <input className="form-control" {...field.input} type={field.type} placeholder={field.placeholder} />
            {hasError && <span className="help-block">{field.meta.error}</span>}
        </div>
    );
};

export const renderHiddenInput = (field: any) => {
    return (
        <input {...field.input} type={field.type} placeholder={field.placeholder} />
    );
};

export const renderTextarea = (field: any) => {
    const hasError = !!field.meta.error && !!field.meta.touched;
    const classNameArray = ['form-group', { 'has-error': hasError }];
    const classNamesResult = classNames(classNameArray);

    return (
        <div className={classNamesResult}>
            <label className="control-label" htmlFor={field.id}>{field.placeholder}</label>
            <textarea className="form-control" {...field.input} placeholder="Content"></textarea>
            {hasError && <span className="help-block">{field.meta.error}</span>}
        </div>
    );
};

export const TextInput = (props) => {
    return <Field component={renderInput} {...props} type="text" />;
};

export const PasswordInput = (props) => {
    return <Field component={renderInput} {...props} type="password" />;
};

export const EmailInput = (props) => {
    return <Field component={renderInput} {...props} type="email" />;
};

export const TextareaInput = (props) => {
    return <Field component={renderTextarea} {...props} />;
};

export const DateInput = (props) => {
    return <Field component={renderInput} {...props} type="date" />;
};

export const HiddenInput = (props) => {
    return <Field component={renderHiddenInput} {...props} type="hidden" />;
};
