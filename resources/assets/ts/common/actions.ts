export interface IAction<T, P> {
    type: T;
    payload?: P;
}

export interface IRequestActionTypes {
    start: string;
    success: string;
    error: string;
}

export const createRequestActionTypes = (actionName: string): IRequestActionTypes => ({
    start: `${actionName}_REQUEST`,
    success: `${actionName}_SUCCESS`,
    error: `${actionName}_ERROR`
});
