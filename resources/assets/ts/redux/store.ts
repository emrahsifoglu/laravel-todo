import { createStore, Store, applyMiddleware, Middleware, StoreEnhancer } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import rootReducer from 'reducers';
import { IStore } from './IStore';

declare module 'redux' {
    export type GenericStoreEnhancer = StoreEnhancer;
}

export function configureStore(history, initialState?: IStore): Store<{}> {

    const middlewares: Middleware[] = [
        routerMiddleware(history),
        thunk
    ];

    if (process.env.NODE_ENV !== 'production') {
        const logger = createLogger({
            level: 'info',
            collapsed: true
        });

        middlewares.push(logger);
    }

    const store = createStore(rootReducer, initialState, composeWithDevTools(
        applyMiddleware(...middlewares)
    ));

    if ((module as any).hot) {
        (module as any).hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers/index').default();
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}
