import { ICounterState } from 'models/counterModel';
import { IAuthState } from 'models/authModel';
import { ITodoState, ITodosState } from 'models/todoModel';

export interface IStore {
    routing: any;
    form: any;
    counter: ICounterState;
    auth: IAuthState;
    todo: ITodoState;
    todos: ITodosState;
}
