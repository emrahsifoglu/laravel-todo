import { IAction } from 'common/actions';

export interface IAuthLoginCreds {
    email: string;
    password: string;
}

export interface IAuthResponse {
    access_token: string;
    expires_in: number;
    token_type: string;
}

export interface IAuthState {
    authenticated: boolean;
    authError: string | null;
    authResponse: IAuthResponse;
    loading: boolean;
}

export interface IAuthAction extends IAction<string, string|IAuthResponse> {
    readonly type: string;
    readonly payload?: string|IAuthResponse;
}
