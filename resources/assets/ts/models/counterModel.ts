import { IAction } from 'common/actions';

export interface ICounter {
    readonly count: number;
}

export interface ICounterStatus {
    readonly isIncrementing: boolean;
    readonly isDecrementing: boolean;
}

export interface ICounterState extends ICounter, ICounterStatus {

}

export interface ICounterAction extends IAction<string, number> {
    readonly type: string;
    readonly value?: number;
}
