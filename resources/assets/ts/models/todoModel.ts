import { IAction } from 'common/actions';

export interface ITodo {
    id?: number;
    user_id: number;
    title: string;
    description: string;
    content: string;
    due_at: Date;
    finished_at: Date|null;
    deleted_at: Date|null;
};

export interface ITodoState {
    todoError: string | null;
    todo: ITodo;
    loading: boolean;
}

export interface ITodosState {
    todosError: string | null;
    todos: ITodo[];
    loading: boolean;
}

export interface ITodoAction extends IAction<string, string|ITodo> {
    readonly type: string;
    readonly payload?: string|ITodo;
}

export interface ITodosAction extends IAction<string, string|any> {
    readonly type: string;
    readonly payload?: string|any;
}
