import * as React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import Counter from 'components/Counter';
import Login from 'components/Login';

const Home = () => <h1>Home</h1>;
const About = () => <h1>About</h1>;

export class Routes extends React.Component<{}, {}> {
    render() {
        return (
            <div className="container">
                <Link to="/">Home</Link>{' '}
                <Link to="/about">About</Link>{' '}
                <Link to="/counter">Counter</Link>{' '}
                <Link to="/login">Login</Link>{' '}
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/counter" component={Counter}/>
                    <Route path="/login" component={Login}/>
                </Switch>
            </div>
        );
    }
}
