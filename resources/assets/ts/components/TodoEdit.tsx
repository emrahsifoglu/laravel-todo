import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch as IDispatch } from 'redux';
import { RouteComponentProps as IRouteComponentProps } from 'react-router-dom';
import { todoGet, todoUpdate } from 'actions/todoActions';
import TodoForm, { ITodoFormData } from 'components/TodoForm';
import { ITodoState, ITodo } from 'models/todoModel';

interface ITodoEditStateProps {
    todo: ITodoState;
}

interface ITodoEditActionProps {
    actions: Actions;
}

interface Actions {
    todoGet: typeof todoGet;
    todoUpdate: typeof todoUpdate;
}

interface ITodoEditProps extends ITodoEditStateProps, ITodoEditActionProps, IRouteComponentProps<any> {

}

class TodoEdit extends React.Component<ITodoEditProps, any> {

    constructor(props) {
        super(props);

        this.props.actions.todoGet(this.props.match.params.id);
    }

    public render() {
        const { todo } = this.props;

        return(
            <div>
            {todo.todo.id > 0
                ? <TodoForm title="Todo Update" button="Update"
                            onSubmit={this.onSubmit.bind(this)}
                            loading={todo.loading}
                            todoError={todo.todoError}
                        />
                : null
            }
            </div>
        );
    }

    private onSubmit(formData: ITodoFormData) {
        this.props.actions.todoUpdate(formData as ITodo);
    }
}

function mapStateToProps(state: ITodoEditStateProps): ITodoEditStateProps {
    return {
        todo: state.todo
    };
}

function mapDispatchToProps(dispatch: IDispatch): any {
    return {
        actions: bindActionCreators({todoGet, todoUpdate}, dispatch)
    };
}

export default connect<{}, {}, any>(
    mapStateToProps,
    mapDispatchToProps
)(TodoEdit);
