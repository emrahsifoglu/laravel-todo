import * as React from 'react';
import { bindActionCreators, Dispatch as IDispatch } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps as IRouteComponentProps, Link } from 'react-router-dom';
import { todoGetAll } from 'actions/todosActions';
import { ITodosState, ITodo } from 'models/todoModel';

interface ITodosStateProps {
    todos: ITodosState;
}

interface ITodosActionProps {
    actions: Actions;
}

interface Actions {
    todoGetAll: typeof todoGetAll;
}

interface ITodosProps extends ITodosStateProps, ITodosActionProps, IRouteComponentProps<any> {

}

class TodoList extends React.Component<ITodosProps, any> {

    constructor(props: ITodosProps) {
        super(props);

        this.props.actions.todoGetAll();
    }

    public render() {
        const { todos } = this.props;

        return (
            <ul>
            { !todos.loading && todos.todos.map((todo: ITodo, i) => (
                <li className="travelcompany-input" key={i}>
                    <Link className="dropdown-item" to={`/admin/todo/${todo.id}`}>{todo.title}</Link>
                </li>
             ))}
            </ul>
        );
    }
}

function mapStateToProps(state: ITodosStateProps): ITodosStateProps {
    return {
        todos: state.todos
    };
}

function mapDispatchToProps(dispatch: IDispatch): ITodosActionProps {
    return {
        actions: bindActionCreators({todoGetAll}, dispatch)
    };
}

export default connect<{}, {}, any>(
    mapStateToProps,
    mapDispatchToProps
)(TodoList);
