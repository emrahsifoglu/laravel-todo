import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps as IRouteComponentProps } from 'react-router-dom';
import { IAuthState } from 'models/authModel';

interface IAuthenticatedStateProps {
    auth: IAuthState;
}

interface IAuthenticatedProps extends IAuthenticatedStateProps, IRouteComponentProps<any> {

}

export function requireAuthentication(Component) {
    class AuthenticatedComponent extends React.Component<IAuthenticatedProps, any> {

        componentWillMount() {
            this.checkAuth(this.props.auth.authenticated);
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth(nextProps.auth.authenticated);
        }

        checkAuth(isAuthenticated) {
            if (!isAuthenticated) {
                this.props.history.push('/login');
            }
        }

        render() {
            return (
                <div>
                    {this.props.auth.authenticated === true
                        ? <Component {...this.props}/>
                        : null
                    }
                </div>
            );
        }
    }

    function mapStateToProps(state: IAuthenticatedStateProps): IAuthenticatedStateProps {
        return {
            auth: state.auth
        };
    }

    return connect<{}, {}, any>(
        mapStateToProps
    )(AuthenticatedComponent);
}
