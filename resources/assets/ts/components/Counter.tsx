import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps as IRouteComponentProps } from 'react-router-dom';
import { incrementRequested, increment, decrementRequested, decrement } from 'actions/counterActions';
import { ICounterState} from 'models/counterModel';

interface ICounterStateProps {
    counter: ICounterState;
}

interface ICounterActionProps {
    actions: Actions;
}

interface ICounterProps extends ICounterStateProps, ICounterActionProps, IRouteComponentProps<any> {

}

interface Actions {
    incrementRequested: typeof incrementRequested;
    increment: typeof increment;
    decrementRequested: typeof decrementRequested;
    decrement: typeof decrement;
}

const Counter: React.StatelessComponent<ICounterProps> = (props) => {
    return (
        <div className="starter-template">
            <h1>Counter</h1>
            <p>Count: {props.counter.count}</p>
            <p>
                <button onClick={() => {
                    props.actions.incrementRequested();
                    props.actions.increment(1);
                }} disabled={props.counter.isIncrementing}>
                    Increment
                </button>
            </p>
            <p>
                <button onClick={() => {
                    props.actions.decrementRequested();
                    props.actions.decrement(1);
                }} disabled={props.counter.isIncrementing}>
                    Decrement
                </button>
            </p>
        </div>
    );
};

function mapStateToProps(state: ICounterStateProps): ICounterStateProps {
    return {
        counter: state.counter
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({incrementRequested, increment, decrementRequested, decrement}, dispatch)
    };
}

export default connect<ICounterStateProps, ICounterActionProps, any>(
    mapStateToProps,
    mapDispatchToProps
)(Counter);
