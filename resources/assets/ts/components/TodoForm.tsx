import * as React from 'react';
import { reduxForm, Form, FormProps, FormErrors, InjectedFormProps } from 'redux-form';
import { connect } from 'react-redux';
import { TextInput, TextareaInput, DateInput, HiddenInput } from 'common/form';
import { ITodo, ITodoState } from 'models/todoModel';

export interface ITodoFormData extends ITodo {
    id?: number;
    title: string;
    description: string;
    content: string;
    due_at: Date;
    finished_at: Date|null;
}

interface ITodoStateProps {
    todo: ITodoState;
    initialValues: ITodo;
}

interface ITodoFormOwnProps {
    title: string;
    button: string;
}

export interface ITodoFormProps extends ITodoStateProps, FormProps<ITodoFormData, {}, {}> {
    onSubmit: (formData: ITodoFormData) => void;
    todoError: string | null;
    loading: boolean;
}

const validate = (formData: ITodoFormData): FormErrors<ITodoFormData> => {
    const { title, description, content, due_at, finished_at} = formData;

    const errors: FormErrors<ITodoFormData> = {};

    if (!title) {
        errors.title = 'Required';
    }

    if (!description) {
        errors.description = 'Required';
    }

    if (!content) {
        errors.content = 'Required';
    }

    if (!due_at) {
        errors.due_at = 'Required';
    }

    return errors;
};

class TodoForm extends React.Component<InjectedFormProps<ITodoFormProps>, {}> {
    public render() {
        const { handleSubmit, submitting, todoError, loading, onSubmit, title, button } = this.props;

        return (
            <div>
                <section>
                    <div id="container_demo" >
                        <div id="wrapper">
                            <div id="todo" className="form">
                                <h1>{title}</h1>
                                <Form className="form-horizontal" onSubmit={ handleSubmit && handleSubmit(onSubmit) }>
                                    { todoError ? this.renderTodoError(todoError) : null }
                                    <HiddenInput id="id" name="id" placeholder="id" disabled={loading} />
                                    <TextInput id="title" name="title" placeholder="Title" disabled={loading} />
                                    <TextareaInput id="description" name="description" placeholder="Description" disabled={loading} />
                                    <TextareaInput id="content" name="content" placeholder="Content" disabled={loading} />
                                    <DateInput id="due_at" name="due_at" placeholder="Due date" disabled={loading} />
                                    <DateInput id="finished_at" name="finished_at" placeholder="Finished date" disabled={loading} />
                                    <p className="change_link todo">
                                        <button className="btn btn-default" type="submit" disabled={loading || submitting}>{button}</button>
                                    </p>
                                </Form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

    private renderTodoError(error: string) {
        return <p className="warning">{error}</p>;
    }
}

function mapStateToProps(state: ITodoStateProps, ownProps: ITodoFormOwnProps) {
    return {
        todo: state.todo,
        initialValues: state.todo.todo
    };
}

const DecoratedTodoForm = reduxForm<TodoForm>({
    form: 'todo',
    validate
})(TodoForm);

export default connect<{}, {}, any>(
    mapStateToProps
)(DecoratedTodoForm);
