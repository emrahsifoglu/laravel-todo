import * as React from 'react';
import { bindActionCreators, Dispatch as IDispatch } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps as IRouteComponentProps } from 'react-router-dom';
import { IAuthState, IAuthLoginCreds } from 'models/authModel';
import { loginUser, resetAuthForm } from 'actions/authActions';
import LoginForm, { ILoginFormData } from './LoginForm';
import { toast } from 'react-toastify';

interface ILoginStateProps {
    auth: IAuthState;
}

interface ILoginActionProps {
    actions: Actions;
}

interface Actions {
    loginUser: typeof loginUser;
    resetAuthForm: typeof resetAuthForm;
}

interface ILoginProps extends ILoginStateProps, ILoginActionProps, IRouteComponentProps<any> {

}

class Login extends React.Component<ILoginProps, any> {

    constructor(props: ILoginProps) {
        super(props);
        this.props.actions.resetAuthForm();
    }

    componentWillReceiveProps(nextProps: ILoginProps) {
        if (nextProps.auth.authenticated) {
            this.props.history.push('/');
        }
    }

    public render() {
        const { auth } = this.props;

        return (
            <LoginForm onSubmit={this.onSubmit.bind(this)}
                       loading={auth.loading}
                       authError={auth.authError}
            />
        );
    }

    private onSubmit(formData: ILoginFormData) {
        this.props.actions.loginUser(formData as IAuthLoginCreds, () => {
            toast.success('You have logged in successfully!', {
                position: 'top-center',
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: false,
                draggablePercent: 0
            });
        });
    }
}

function mapStateToProps(state: ILoginStateProps): ILoginStateProps {
    return {
        auth: state.auth
    };
}

function mapDispatchToProps(dispatch: IDispatch): ILoginActionProps {
    return {
        actions: bindActionCreators({loginUser, resetAuthForm}, dispatch)
    };
}

export default connect<{}, {}, any>(
    mapStateToProps,
    mapDispatchToProps
)(Login);
