import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import { requireAuthentication } from 'components/Authenticated';
import NavBar from 'components/NavBar';
import Counter from 'components/Counter';
import Login from 'components/Login';
import TodoEdit from 'components/TodoEdit';
import TodoAdd from 'components/TodoAdd';
import TodoList from 'components/TodoList';

const Main = () => (
    <main role="main" className="container">
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/counter" component={Counter}/>
            <Route path="/login" component={Login}/>
            <Route path="/admin/todo/:id" component={requireAuthentication(TodoEdit)}/>
            <Route path="/admin/todo" component={requireAuthentication(TodoAdd)}/>
            <Route path="/admin/todos" component={requireAuthentication(TodoList)}/>
        </Switch>
    </main>
);

const Home = () => <div className="starter-template">
    <h1>Home</h1>
    <button onClick={() => {
        toast.info('🦄 Wow so easy!', {
            position: 'top-center',
            autoClose: 1500,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: false,
            draggablePercent: 0
        });
    }}>Notify !</button>
</div>;

const About = () => <div className="starter-template"><h1>About</h1></div>;

class App extends React.Component<{}, {}> {
    render() {
        return (
            <div>
                <NavBar />
                <ToastContainer />
                <Main />
            </div>
        );
    }
}

export default App;
