import * as React from 'react';
import { bindActionCreators, Dispatch as IDispatch } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps as IRouteComponentProps } from 'react-router-dom';
import { ITodo, ITodoState } from 'models/todoModel';
import { todoAdd, resetTodoForm } from 'actions/todoActions';
import TodoForm, { ITodoFormData } from './TodoForm';

interface ITodoAddStateProps {
    todo: ITodoState;
}

interface ITodoAddActionProps {
    actions: Actions;
}

interface Actions {
    todoAdd: typeof todoAdd;
    resetTodoForm: typeof resetTodoForm;
}

interface ITodoAddProps extends ITodoAddStateProps, ITodoAddActionProps, IRouteComponentProps<any> {

}

class TodoAdd extends React.Component<ITodoAddProps, any> {

    constructor(props: ITodoAddProps) {
        super(props);
        this.props.actions.resetTodoForm();
    }

    public render() {
        const { todo } = this.props;

        return (
            <TodoForm title="Todo Add" button="Save" onSubmit={this.onSubmit.bind(this)}
                    loading={todo.loading}
                    todoError={todo.todoError}
            />
        );
    }

    private onSubmit(formData: ITodoFormData) {
        this.props.actions.todoAdd(formData as ITodo);
    }
}

function mapStateToProps(state: ITodoAddStateProps): ITodoAddStateProps {
    return {
        todo: state.todo
    };
}

function mapDispatchToProps(dispatch: IDispatch): ITodoAddActionProps {
    return {
        actions: bindActionCreators({todoAdd, resetTodoForm}, dispatch)
    };
}

export default connect<{}, {}, any>(
    mapStateToProps,
    mapDispatchToProps
)(TodoAdd);
