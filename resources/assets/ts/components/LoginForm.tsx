import * as React from 'react';
import { reduxForm, FormProps, FormErrors, InjectedFormProps } from 'redux-form';
import { PasswordInput, EmailInput } from 'common/form';

export interface ILoginFormData {
    email?: string;
    password?: string;
}

export interface ILoginFormProps extends FormProps<ILoginFormData, {}, {}> {
    onSubmit: (formData: ILoginFormData) => void;
    authError: string | null;
    loading: boolean;
}

const validate = (formData: ILoginFormData): FormErrors<ILoginFormData> => {
    const { email, password } = formData;

    const errors: FormErrors<ILoginFormData> = {};

    if (!email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
        errors.email = 'Invalid email address';
    }

    if (!password) {
        errors.password = 'Required';
      } else if (password.length < 6) {
        errors.password = 'Must be at least 6 characters';
    }

    return errors;
};

class LoginForm extends React.Component<InjectedFormProps<ILoginFormProps>, {}> {
    public render() {
        const { handleSubmit, submitting, authError, loading, onSubmit } = this.props;

        return (
            <div>
                <section>
                    <div id="container_demo" >
                        <div id="wrapper">
                            <div id="login" className="form">
                                <h1>Log in</h1>
                                <form className="form-horizontal" onSubmit={ handleSubmit && handleSubmit(onSubmit) }>
                                    { authError ? this.renderAuthError(authError) : null }
                                    <EmailInput id="email" name="email" placeholder="Email" disabled={loading} />
                                    <PasswordInput id="password" name="password" placeholder="Password" disabled={loading} />
                                    <p className="change_link login">
                                        <button className="btn btn-default" type="submit" disabled={loading || submitting}>Submit</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }

    private renderAuthError(error: string) {
        return <p className="warning">{error}</p>;
    }
}

export default reduxForm({
    form: 'login',
    validate
})(LoginForm);
