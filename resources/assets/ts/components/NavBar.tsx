import * as React from 'react';
import { bindActionCreators, Dispatch as IDispatch } from 'redux';
import { connect } from 'react-redux';
import { Link, RouteComponentProps as IRouteComponentProps } from 'react-router-dom';
import { IAuthState } from 'models/authModel';
import { logoutUser } from 'actions/authActions';
import { toast } from 'react-toastify';

interface INavBarStateProps {
  auth: IAuthState;
}

interface INavBarActionProps {
  actions: Actions;
}

interface Actions {
  logoutUser: typeof logoutUser;
}

interface INavbarProps extends INavBarStateProps, INavBarActionProps, IRouteComponentProps<any> {

}

class NavBar extends React.Component<INavbarProps, any> {
  render() {

    const { auth, actions }  = this.props;

    return (
      <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <Link className="navbar-brand" to="/">Home</Link>
        <button className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault"
                aria-expanded="false"
                aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className="nav-link" to="/about">About</Link>{' '}
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/counter">Counter</Link>{' '}
            </li>              
            { auth.authenticated &&
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle"
                  href="#"
                  id="dropdownAdmin"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false">Admin</a>
              <div className="dropdown-menu" aria-labelledby="dropdownAdmin">
                <Link className="dropdown-item" to="/admin/todos">Todos</Link>
                <Link className="dropdown-item" to="/admin/todo">Todo add</Link>
              </div>
            </li> }
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li className="nav-item">
              { auth.authenticated
                ? <a href="#" onClick={() => actions.logoutUser(
                      () => { toast.info('👋 Goodbye...', {
                          position: 'top-center',
                          autoClose: 1500,
                          hideProgressBar: true,
                          closeOnClick: true,
                          pauseOnHover: false,
                          draggable: false,
                          draggablePercent: 0
                      });
                    })
                  }>Logout</a>
                : <Link className="nav-link" to="/login">Login</Link>
              }
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

function mapStateToProps(state: INavBarStateProps): INavBarStateProps {
  return {
      auth: state.auth
  };
}

function mapDispatchToProps(dispatch: IDispatch): INavBarActionProps {
  return {
      actions: bindActionCreators({logoutUser}, dispatch)
  };
}

export default connect<{}, {}, any>(
  mapStateToProps,
  mapDispatchToProps
)(NavBar);
