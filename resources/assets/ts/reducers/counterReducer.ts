import { INCREMENT_REQUESTED, INCREMENT, DECREMENT_REQUESTED, DECREMENT } from 'actions/actionTypes';
import { ICounterAction, ICounterState } from 'models/counterModel';

const initialCounterState: ICounterState = {
    count: 0,
    isIncrementing: false,
    isDecrementing: false
};

export function counterReducer(state = initialCounterState, action: ICounterAction): ICounterState {
    switch (action.type) {
        case INCREMENT_REQUESTED:
            return {
                ...state,
                isIncrementing: true
            };
        case INCREMENT:
            return {
                ...state,
                count: state.count + action.payload,
                isIncrementing: !state.isIncrementing
            };
        case DECREMENT_REQUESTED:
            return {
                ...state,
                isDecrementing: true
            };
        case DECREMENT:
            return {
                ...state,
                count: state.count - action.payload,
                isDecrementing: !state.isDecrementing
            };
        default:
            return state;
    }
}
