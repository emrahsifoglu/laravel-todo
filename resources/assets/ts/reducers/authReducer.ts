
import { AUTHENTICATE_USER, RESET_AUTH_FORM, UNAUTHENTICATE_USER, authFormSubmitActionTypes } from 'actions/actionTypes';
import { IAuthState, IAuthAction, IAuthResponse } from 'models/authModel';
import { AUTH_ACCESS_TOKEN } from 'common/consts';

const initialAuthResponse: IAuthResponse = {
    access_token: null,
    expires_in: null,
    token_type: null
};

const initialAuthState: IAuthState = {
    authenticated: !!localStorage.getItem(AUTH_ACCESS_TOKEN),
    authResponse: initialAuthResponse,
    authError: null,
    loading: false
};

export function authReducer(state: IAuthState = initialAuthState, action: IAuthAction): IAuthState {
    switch (action.type) {
        case AUTHENTICATE_USER:
            return {
                ...state,
                authenticated: true,
                authResponse: action.payload as IAuthResponse
            };
        case UNAUTHENTICATE_USER:
            return {
                ...state,
                authenticated: false,
                authResponse: initialAuthResponse
            };
        case RESET_AUTH_FORM:
            return {
                ...state,
                authError: null,
                loading: false
            };
        case authFormSubmitActionTypes.start:
            return {
                ...state,
                authError: null,
                loading: true
            };
        case authFormSubmitActionTypes.success:
            return {
                ...state,
                authError: null,
                loading: false
            };
        case authFormSubmitActionTypes.error:
            return {
                ...state,
                authError: action.payload as string,
                loading: false
            };
        default:
            return state;
    }
}
