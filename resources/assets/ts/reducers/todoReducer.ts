import { ITodoAction, ITodoState, ITodo } from 'models/todoModel';
import { todoFormSubmitActionTypes, todoRequestSentActionTypes, RESET_TODO_FORM, SHOW_TODO, UPDATE_TODO } from 'actions/actionTypes';

const initialTodo: ITodo = {
    user_id: -1,
    title: null,
    description: null,
    content: null,
    due_at: null,
    finished_at: null,
    deleted_at: null
};

const initialTodoState: ITodoState = {
    todoError: null,
    todo: initialTodo,
    loading: false
};

export function todoReducer(state = initialTodoState, action: ITodoAction): ITodoState {
    switch (action.type) {
        case RESET_TODO_FORM:
            return {
                ...state,
                todo: initialTodo,
                todoError: null,
                loading: false
            };
        case SHOW_TODO:
            return {
                ...state,
                todo: action.payload as ITodo
            };
        case UPDATE_TODO:
            return {
                ...state,
                todo: action.payload as ITodo
            };
        case todoRequestSentActionTypes.start:
            return {
                ...state,
                todo: initialTodo,
                todoError: null,
                loading: true
            };
        case todoFormSubmitActionTypes.start:
            return {
                ...state,
                todoError: null,
                loading: true
            };
        case todoRequestSentActionTypes.success:
        case todoFormSubmitActionTypes.success:
            return {
                ...state,
                todoError: null,
                loading: false
            };
        case todoRequestSentActionTypes.error:
        case todoFormSubmitActionTypes.error:
            return {
                ...state,
                todoError: action.payload as string,
                loading: false
            };
        default:
            return state;
    }
}
