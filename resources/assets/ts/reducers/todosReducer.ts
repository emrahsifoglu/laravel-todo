import { ITodosAction, ITodosState, ITodo } from 'models/todoModel';
import { SHOW_TODO_ALL, todosRequestSentActionTypes } from 'actions/actionTypes';

const initialTodos: ITodo[] = [];

const initialTodosState: ITodosState = {
    todosError: null,
    todos: initialTodos,
    loading: false
};

export function todosReducer(state = initialTodosState, action: ITodosAction): ITodosState {
    switch (action.type) {
        case SHOW_TODO_ALL:
            return {
                ...state,
                todos: action.payload.success.data as ITodo[]
            };
        case todosRequestSentActionTypes.start:
            return {
                ...state,
                todosError: null,
                loading: true
            };
        case todosRequestSentActionTypes.success:
            return {
                ...state,
                todosError: null,
                loading: false
            };
        case todosRequestSentActionTypes.error:
            return {
                ...state,
                todosError: action.payload as string,
                loading: false
            };
        default:
            return state;
    }
}
