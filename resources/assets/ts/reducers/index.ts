import { Reducer, combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer} from 'redux-form';
import { counterReducer } from './counterReducer';
import { authReducer } from './authReducer';
import { todoReducer } from './todoReducer';
import { todosReducer } from './todosReducer';
import { IStore } from 'redux/IStore';

const rootReducer: Reducer<IStore> = combineReducers<IStore>({
    routing: routerReducer,
    form: formReducer,
    counter: counterReducer,
    auth: authReducer,
    todo: todoReducer,
    todos: todosReducer
});

export default rootReducer;
