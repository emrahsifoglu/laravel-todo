import { request, RequestOptions } from 'http/request';
import { IDictionary } from 'common/data';

export class Http {

    public static post(url: string, data?: IDictionary<any>, opts?: RequestOptions): Promise<any> {
        return request('POST', url, data, opts);
    }

    public static get(url: string, data?: IDictionary<any>, opts?: RequestOptions): Promise<any> {
        return request('GET', url, data, opts);
    }

    public static put(url: string, data?: IDictionary<any>, opts?: RequestOptions): Promise<any> {
        return request('POST', url, {...data, _method: 'PUT' }, opts);
    }
}
