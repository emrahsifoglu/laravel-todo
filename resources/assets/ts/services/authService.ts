import { Http } from './http';
import { RequestOptions } from 'http/request';
import { IAuthLoginCreds, IAuthResponse } from 'models/authModel';

export class AuthService {

    public static login(url: string, data: IAuthLoginCreds, opts?: RequestOptions): Promise<any> {
        let authResponse: IAuthResponse;

        return Http.post(url, data, opts).then((response) => {
            authResponse = response.data;

            return authResponse;
        });
    }

}
