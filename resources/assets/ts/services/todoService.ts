import { Http } from './http';
import { RequestOptions } from 'http/request';
import { ITodo } from 'models/todoModel';

export class TodoService {

    public static add(url: string, data: ITodo, opts?: RequestOptions): Promise<any> {
        return Http.post(url, data, opts).then((response) => {
            const todo: ITodo = response.data;

            return todo;
        });
    }

    public static get(url: string, opts?: RequestOptions): Promise<any> {
        return Http.get(url, null, opts).then((response) => {
            const todo: ITodo = response.data;

            return todo;
        });
    }

    public static getAll(url: string, opts?: RequestOptions): Promise<any> {
        return Http.get(url, null, opts).then((response) => {
            const todos: ITodo[] = response.data;

            return todos;
        });
    }

    public static update(url: string, data: ITodo, opts?: RequestOptions): Promise<any> {
        return Http.put(url, data, opts).then((response) => {
            const todo: ITodo = response.data;

            return todo;
        });
    }
}
