import { Dispatch as IDispatch} from 'redux';
import { reset } from 'redux-form';
import { TodoService } from 'services/todoService';
import {
    todoFormSubmitActionTypes, todoRequestSentActionTypes,
    RESET_TODO_FORM, SHOW_TODO, UPDATE_TODO
} from 'actions/actionTypes';
import { ITodoAction, ITodo } from 'models/todoModel';
import { AUTH_ACCESS_TOKEN } from 'common/consts';

export const resetTodoForm = () => {
    return (dispatch: IDispatch) => {
        dispatch<ITodoAction>(reset('todo'));
        dispatch<ITodoAction>({
            type: RESET_TODO_FORM
        });
    };
};

export const showTodo = (todo: ITodo) => {
    return (dispatch: IDispatch) => {
        dispatch<ITodoAction>({
            type: SHOW_TODO,
            payload: todo
        });
    };
};

export const updateTodo = (todo: ITodo) => {
    return (dispatch: IDispatch) => {
        dispatch<ITodoAction>({
            type: UPDATE_TODO,
            payload: todo
        });
    };
};

export const todoFormStart: ITodoAction = {
    type: todoFormSubmitActionTypes.start
};

export const todoFormSuccess: ITodoAction = {
    type: todoFormSubmitActionTypes.success
};

export const todoFormError = (error: string): ITodoAction => ({
    type: todoFormSubmitActionTypes.error,
    payload: error
});

export const todoRequestStart: ITodoAction = {
    type: todoRequestSentActionTypes.start
};

export const todoRequestSuccess: ITodoAction = {
    type: todoRequestSentActionTypes.success
};

export const todoRequestError = (error: string): ITodoAction => ({
    type: todoRequestSentActionTypes.error,
    payload: error
});

export const todoGet = (id: number) => {
    return (dispatch: IDispatch) => {
        dispatch<ITodoAction>(todoRequestStart);

        return TodoService.get(`http://192.168.222.128:8000/api/todo/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem(AUTH_ACCESS_TOKEN)}`
            }
        })
        .then((todo: ITodo) => {
            dispatch<any>(showTodo(todo));
            dispatch<ITodoAction>(todoRequestSuccess);
        }).catch((err: any) => {
            dispatch<ITodoAction>(todoRequestError(err.data.message));
        });
    };
};

export const todoAdd = (todo: ITodo) => {
    return (dispatch: IDispatch) => {
        todo.user_id = 1;
        dispatch<ITodoAction>(todoFormStart);

        return TodoService.add('http://192.168.222.128:8000/api/todo', todo, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem(AUTH_ACCESS_TOKEN)}`
            }
        })
        .then((todo: ITodo) => {
            dispatch<any>(resetTodoForm());
            dispatch<ITodoAction>(todoFormSuccess);
        }).catch((err: any) => {
            dispatch<ITodoAction>(todoFormError(err.data.message));
        });
    };
};

export const todoUpdate = (todo: ITodo) => {
    return (dispatch: IDispatch) => {
        dispatch<ITodoAction>(todoFormStart);

        return TodoService.update(`http://192.168.222.128:8000/api/todo/${todo.id}`, todo, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem(AUTH_ACCESS_TOKEN)}`
            }
        })
        .then((todo: ITodo) => {
            dispatch<any>(updateTodo(todo));
            dispatch<ITodoAction>(todoFormSuccess);
        }).catch((err: any) => {
            dispatch<ITodoAction>(todoFormError(err.data.message));
        });
    };
};
