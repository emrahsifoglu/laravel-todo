import { createRequestActionTypes } from 'common/actions';

// Counter
export const INCREMENT_REQUESTED = 'counter/INCREMENT_REQUESTED';
export const INCREMENT = 'counter/INCREMENT';
export const DECREMENT_REQUESTED = 'counter/DECREMENT_REQUESTED';
export const DECREMENT = 'counter/DECREMENT';

// Authentian
export const AUTHENTICATE_USER = 'auth/AUTHENTICATE_USER';
export const UNAUTHENTICATE_USER = 'auth/UNAUTHENTICATE_USER';
export const RESET_AUTH_FORM = 'auth/RESET_AUTH_FORM';
export const authFormSubmitActionTypes = createRequestActionTypes('auth/FORM_SUBMIT');

// Todo
export const RESET_TODO_FORM = 'todo/RESET_TODO_FORM';
export const SHOW_TODO = 'todo/SHOW';
export const SHOW_TODO_ALL = 'todo/SHOW_TODO_ALL';
export const UPDATE_TODO = 'todo/UPDATE';
export const todoFormSubmitActionTypes = createRequestActionTypes('todo/FORM_SUBMIT');
export const todoRequestSentActionTypes = createRequestActionTypes('todo/REQUEST_SENT');
export const todosRequestSentActionTypes = createRequestActionTypes('todos/REQUEST_SENT');
