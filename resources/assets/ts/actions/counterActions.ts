import { INCREMENT_REQUESTED, INCREMENT, DECREMENT_REQUESTED, DECREMENT } from 'actions/actionTypes';
import { ICounterAction } from 'models/counterModel';

export function incrementRequested(): ICounterAction {
    return {
        type: INCREMENT_REQUESTED
    };
}

export function increment(n: number): ICounterAction {
    return {
        type: INCREMENT,
        payload: n
    };
}

export function decrementRequested(): ICounterAction {
    return {
        type: DECREMENT_REQUESTED
    };
}

export function decrement(n: number): ICounterAction {
    return {
        type: DECREMENT,
        payload: n
    };
}
