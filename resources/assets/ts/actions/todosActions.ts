import { Dispatch as IDispatch} from 'redux';
import { TodoService } from 'services/todoService';
import {
    todosRequestSentActionTypes,
    SHOW_TODO_ALL
} from 'actions/actionTypes';
import { ITodoAction, ITodo, ITodosAction } from 'models/todoModel';
import { AUTH_ACCESS_TOKEN } from 'common/consts';

export const showTodos = (todos: ITodo[]) => {
    return (dispatch: IDispatch) => {
        dispatch<ITodosAction>({
            type: SHOW_TODO_ALL,
            payload: todos
        });
    };
};

export const todosRequestStart: ITodoAction = {
    type: todosRequestSentActionTypes.start
};

export const todosRequestSuccess: ITodoAction = {
    type: todosRequestSentActionTypes.success
};

export const todosRequestError = (error: string): ITodoAction => ({
    type: todosRequestSentActionTypes.error,
    payload: error
});

export const todoGetAll = () => {
    return (dispatch: IDispatch) => {
        dispatch<ITodosAction>(todosRequestStart);

        return TodoService.get('http://192.168.222.128:8000/api/todo/', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem(AUTH_ACCESS_TOKEN)}`
            }
        })
        .then((todos: ITodo[]) => {
            dispatch<any>(showTodos(todos));
            dispatch<ITodosAction>(todosRequestSuccess);
        }).catch((err: any) => {
            dispatch<ITodosAction>(todosRequestError(err.data.message));
        });
    };
};
