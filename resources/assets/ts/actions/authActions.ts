import { RESET_AUTH_FORM } from './actionTypes';
import { reset } from 'redux-form';
import { Dispatch as IDispatch} from 'redux';
import { AuthService } from 'services/authService';
import { AUTHENTICATE_USER, UNAUTHENTICATE_USER, authFormSubmitActionTypes } from 'actions/actionTypes';
import { IAuthAction, IAuthLoginCreds, IAuthResponse } from 'models/authModel';
import { AUTH_ACCESS_TOKEN } from 'common/consts';

export const resetAuthForm = () => {
    return (dispatch: IDispatch) => {
        dispatch<IAuthAction>(reset('login'));
        dispatch<IAuthAction>({
            type: RESET_AUTH_FORM
        });
    };
};

export const authenticateUser = (authResponse: IAuthResponse): IAuthAction => ({
    type: AUTHENTICATE_USER,
    payload: authResponse
});

export const unauthenticateUser = (): IAuthAction => ({
    type: UNAUTHENTICATE_USER
});

export const authRequestStart: IAuthAction = {
    type: authFormSubmitActionTypes.start
};

export const authRequestSuccess: IAuthAction = {
    type: authFormSubmitActionTypes.success
};

export const authRequestError = (error: string): IAuthAction => ({
    type: authFormSubmitActionTypes.error,
    payload: error
});

export const loginUser = ({ email, password }: IAuthLoginCreds, success?: () => void) => {
    return (dispatch: IDispatch) => {
        dispatch<IAuthAction>(authRequestStart);

        return AuthService.login('http://192.168.222.128:8000/api/login', {
            email,
            password
        })
        .then((authResponse: IAuthResponse) => {
            localStorage.setItem(AUTH_ACCESS_TOKEN, authResponse.access_token);
            dispatch<IAuthAction>(authenticateUser(authResponse));
            dispatch<IAuthAction>(authRequestSuccess);
            success();
        }).catch((err: any) => {
            dispatch<IAuthAction>(authRequestError(err.data.message));
        });
    };
};

export const logoutUser = (success?: () => void) => {
    return (dispatch: IDispatch) => {
        localStorage.removeItem(AUTH_ACCESS_TOKEN);
        dispatch<IAuthAction>(unauthenticateUser());
        success();
    };
};
