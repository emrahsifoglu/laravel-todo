const { DefinePlugin, optimize } = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const common = require('./webpack.config.common');

module.exports = merge(common, {
    devtool: false,
    entry: [
        './index.tsx',
    ],
    plugins: [
        new DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new CleanWebpackPlugin(['public/js', 'public/css'], {}),
        new optimize.OccurrenceOrderPlugin(),
        new optimize.ModuleConcatenationPlugin(),
        new optimize.UglifyJsPlugin({
            compressor: {
                screw_ie8: true,
                warnings: false
            }
        }),
        new CompressionPlugin({
            algorithm: 'gzip',
            test: /\.js$/,
            minRatio: 0.8
        })
    ],
});
