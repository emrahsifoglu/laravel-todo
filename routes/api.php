<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::middleware(['api'])->group(function () {
    Route::post('login', 'Api\Auth\AuthController@login')->name('api.login');
    Route::post('logout', 'Api\Auth\AuthController@logout')->name('api.logout');
    Route::post('refresh', 'Api\Auth\AuthController@refresh')->name('api.refresh');
    Route::post('me', 'Api\Auth\AuthController@me')->name('api.admin.me');
    Route::post('todo', 'Api\TodoController@store')->name('todo.store');
    Route::get('todo', 'Api\TodoController@index')->name('todo.index');
    Route::get('todo/{todo}', 'Api\TodoController@show')->name('todo.show');
    Route::match(['put', 'patch', 'post'], 'todo/{todo}', 'Api\TodoController@update')->name('todo.update');
    Route::delete('todo/{todo}', 'Api\TodoController@delete')->name('todo.delete');
});
