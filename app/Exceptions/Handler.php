<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if (!$request->expectsJson() || substr($request->path(), 0, 3) !== 'api') {
            return parent::render($request, $exception);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->json(['message' => 'Not Found'], 404);
        } else if ($exception instanceof ModelNotFoundException) {
            return response()->json(['message' => 'Model Not Found'], 400);
        } else if ($exception instanceof TokenExpiredException) {
            return response()->json(['message' => 'Token is expired'], 404);
        } else if($exception instanceof TokenInvalidException) {
            return response()->json(['message' => 'Token is invalid'], 404);
        } else if($exception instanceof JWTException) {
            return response()->json(['message' => 'Undefined token error'], 404);
        }

        return parent::render($request, $exception);
    }
}
