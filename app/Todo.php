<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'description', 'content', 'due_at', 'finished_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['due_at', 'finished_at', 'deleted_at'];

    /**
     * Relationship between todos and user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Load only todos related with the user id
     *
     * @param $query
     * @param $userId
     *
     * @return mixed
     */
    public function scopeMine($query, $userId)
    {
        return $query->where('user_id', $userId);
    }

    /**
     * Load all for admin and paginate
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function loadAll()
    {
        return static::latest()
            ->paginate();
    }

    /**
     * Load all for logged in user and paginate
     *
     * @param $userId
     *
     * @return mixed
     */
    public static function loadAllBelongsTo($userId)
    {
        return static::latest()
            ->mine($userId)
            ->paginate();
    }
}
