<?php

namespace App\Observers;

use App\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     *
     * @return void
     */
    public function creating(User $user)
    {
        $user->password = bcrypt($user->password);
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User  $user
     *
     * @return void
     */
    public function deleting(User $user)
    {
        // Todo: implement deleting method
    }
}
