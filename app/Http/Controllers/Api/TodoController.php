<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TodoRequest;
use App\Http\Resources\TodoCollection;
use App\Http\Resources\TodoResource;
use App\Todo;
use App\User;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Create a new TodoController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return LengthAwarePaginator|mixed
     */
    public function index(Request $request)
    {
        $todos = ($request->user()->is_admin)
            ? Todo::loadAll()
            : Todo::loadAllBelongsTo($request->user()->id);

        return response()->json(['success' => $todos], 200);
    }

    /**
     * Show the specified todo from storage.
     *
     * @param  \App\Todo  $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        return response()->json(new TodoResource($todo), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TodoRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
        try {
            $data = $this->validateTodoRequest($request);

            $todo = new Todo($data);
            $todo->user_id = (int)$data['user_id'];

            if($todo->save() !== true) {
                return response(null, 422);
            }

            return response()->json(new TodoResource($todo), 201);
        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Update the specified todo in storage.
     *
     * @param  TodoRequest  $request
     * @param  \App\Todo  $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request, Todo $todo)
    {
        try {
            $data = $this->validateTodoRequest($request);

            $todo->user_id = (int)$data['user_id'];

            if ($todo->update($data) !== true) {
                return response(null, 422);
            }

            return response()->json(new TodoResource($todo), 200);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified todo from storage.
     *
     * @param  \App\Todo  $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Todo $todo)
    {
        try {
            if ($todo->delete() !== true) {
                return response(null, 422);
            }

            return response(null, 204);
        } catch (Exception $e) {
            return response([
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * @param  TodoRequest  $request
     *
     * @return array
     */
    private function validateTodoRequest(TodoRequest $request) {
        $data = $request->validated();

        $dueAt = \Carbon\Carbon::createFromFormat('Y-m-d', $data['due_at']);
        $data['due_at'] = $dueAt;

        if (!empty($data['finished_at'])) {
            $finishedAt = \Carbon\Carbon::createFromFormat('Y-m-d', $data['finished_at']);
            $data['finished_at'] = $finishedAt;
        }

        return $data;
    }
}
