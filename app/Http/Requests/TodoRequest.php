<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:1',
            'description' => 'required|min:1',
            'content' => 'required|min:1',
            'due_at' => 'required|date|date_format:Y-m-d|before_or_equal:finished_at',
            'finished_at' => 'nullable|date|date_format:Y-m-d|after_or_equal:due_at',
            'user_id' => 'required|exists:users,id',
        ];
    }
}
