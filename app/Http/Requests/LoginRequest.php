<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];
    }

    /**
     * Set the validation error messages.
     *
     * @return array
     */
    public function messages() {
        return [
            'email.required' => 'Emails is required.',
            'email.email' => 'Emails is incorrect.',
            'password.required' => 'Password is incorrect.',
            'password.min' => 'Password have to be at least 6 characters long.',
        ];
    }
}
