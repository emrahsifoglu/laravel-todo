<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $todo = parent::toArray($request);

        $todo['due_at'] = date('Y-m-d', strtotime($this->due_at));
        $todo['finished_at'] = $this->normalizeDate($this->finished_at);
        $todo['created_at'] = $this->normalizeDate($this->created_at);
        $todo['updated_at'] = $this->normalizeDate($this->updated_at);
        $todo['deleted_at'] = $this->normalizeDate($this->deleted_at);

        return $todo;
    }

    private function normalizeDate($date) {
        return ($date) ? date('Y-m-d', strtotime($date)) : null;
    }
}
