const { resolve, join } = require('path');
const { NoEmitOnErrorsPlugin, NamedModulesPlugin, ProvidePlugin } = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const NotifierPlugin = require('webpack-notifier');

// [HERE] a helper function for alias definitions
function asPath(srcSubDir) {
    return join(__dirname, 'resources/assets/ts', srcSubDir);
}

module.exports = {
    context: resolve(__dirname, 'resources/assets/ts'),
    output: {
        filename: 'js/app.js',
        path: resolve(__dirname, 'public'),
        publicPath: '/'
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
        modules: [resolve('./resources/assets/ts'), resolve('node_modules')],
        alias: {
            '$actions': asPath('actions/*'),
            '$common': asPath('common/*'),
            '$components': asPath('components/*'),
            '$http': asPath('http/*'),
            '$models': asPath('models/*'),
            '$reducers': asPath('reducers/*'),
            '$redux': asPath('redux/*'),
            '$services': asPath('services/*')
        }
    },
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.tsx?$/,
                loader: 'tslint-loader',
                options: {
                    failOnHint: false,
                    configuration: require('./tslint.json')
                },
                exclude: [resolve(__dirname, "node_modules")],
            }, {
                test: /\.tsx?$/,
                loaders: 'awesome-typescript-loader',
                exclude: [resolve(__dirname, "node_modules")],
            }, {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            }, {
                test: /(\.css|\.scss)$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader?modules&importLoaders=1&localIdentName=[local]',
                        options: {
                            url: false,
                            minimize: false,
                            sourceMap: true
                        }
                    }, {
                        loader: 'postcss-loader',
                        options: { options: { sourceMap: true } } 
                    }, {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }]
                })
            }, {
                test: /\.png$/,
                loader: "url-loader?limit=100000"
            }, {
                test: /\.jpg$/,
                loader: "file-loader"
            }, {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader'
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            }
        ]
    },
    plugins: [
        new NotifierPlugin({title: 'Webpack'}),
        new ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
        }),
        new NamedModulesPlugin(),
        new NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin('css/app.css', {
            publicPath: '/',
            allChunks: true
        }),
    ],
};
