const { resolve } = require('path');
const { DefinePlugin, HotModuleReplacementPlugin } = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const common = require('./webpack.config.common');

const host = process.env.HOST || 'localhost';

module.exports = merge(common, {
    devtool: 'inline-source-map',
    context: resolve(__dirname, 'resources/assets/ts'),
    entry: [
        `webpack-dev-server/client?http://${host}:8080/`,
        'webpack/hot/only-dev-server',
        'react-hot-loader/patch',
        './index.tsx',
    ],
    plugins: [
        new DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
        new HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: resolve(__dirname, 'resources/assets/template/index.html'),
        })
    ],
    devServer: {
        historyApiFallback: true,
        port: '8080',
        hot: true,
        host: `${host}`,
        noInfo: true,
        quiet: false,
        contentBase: resolve(__dirname, 'public'),
        publicPath: '/'
    },
});
